# Silverio Ibañez Omar 17161247 PLF
## Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?
```plantuml
@startmindmap
* Programando ordenadores \nen los 80 y ahora.
** Antes
*** Ordenadores Antiguos
**** Dispositivos que ya no se comercializan
**** Menores recursos
***** Memoria
***** CPU
***** Potencia
**** Complejos
*** Programación
**** Lenguaje ensamblador
**** Requisitos
***** Conocer Hardware
***** Conocer espacios de memoria
***** Conocer ciclos de reloj
***** Conocer arquitectura del ordenador
****** Cada una eran distintas
****** Cada una se programaba diferente
*** Desventajas
**** Limitación por el hardware
**** Menos potencia
**** Programas mas dificiles de entender
**** Programas demasiados largos
*** Caracteristicas
**** Control total del hardware
**** Fallos por razones humanas
**** Se conocia a fondo la maquina
**** Mayor codigo util


** Ahora
*** Ordenadores actuales
**** Mas graficos
**** Dispositivos
***** Consolas de videojuegos
***** Celulares
***** Computadoras
**** Mayor capacidad de recursos
***** Memoria
***** Tarjeta grafica
***** CPU
**** Se cormecializan en cualquier tienda
*** Programación
**** Mas entendible
**** Multiplataforma
**** Lenguajes de alto nivel
***** Ejemplo Java
****** Dispositivos compatibles
******* Celulares
******* Computadoras
******* Electrodomesticos
****** Utiliza Maquina virtual
**** Requisitos
***** Conocer distintos lenguajes de alto nivel
***** Contar con diferentes librerias y dependencias
*** Desventajas
**** Menos eficiente
**** Mayor sobrecarga de llamadas al cpu
**** Bugs inevitables
**** Mayor integración de parches
**** Mucho codigó basura
**** Menos seguros
*** Caracateristicas
**** Facilidad de programar software complejo
**** Bugs
***** Parte del codigo
***** Parte humano
**** Facilidad para reciclar el programa
@endmindmap
```
## Historia de los algoritmos y de los lenguajes de programación
```plantuml
@startmindmap
* Historia de los algoritmos y de los lenguajes de programación
** Algoritmos
*** Descripción
**** Lista de Operaciones
***** Ordenada
***** Finita
**** permite
***** Hallar solución a problemas
****** dado un
******* Estado Inicial
******* Una Entrada
**** Historia
***** Primeros registros
****** Mesopotamia
******* Algoritmos para transacciones comerciales
****** Siglo xvii
******* Aperecen ayudas mecanicas para los calculos
****** siglo 19
******* Primeras maquinas programables
****** siglo xx
******* Algoritmos mas desarrollados
****** 1960
******* Surguen los lenguajes de programación
******** escribir los algoritmos
******** Ejecutar algoritmos
**** Tipos de impresión de algoritmos en la historia
***** Tablillas de arcilla
****** Encontradas en mesopotamia
***** Cartón
****** Usadas para el tellar de jhon (primeras maquinas automatizadas)
****** Tarjetas perforadas
**** Propiedades
***** Tipos de algoritmos
****** Razonables o Polinomiales
******* Crecen despacio a medidda que los problemas se hacen mas grande
****** No razonables o Exponenciales
******* Algoritmos con mal comportamiento

** Lenguajes de Programación
*** Paradigmas o Familia de lenguajes
*** Permiten implementar algoritmos
*** Tipos de lenguajes de programación
**** Imperativo
***** Ejemplo Fortran
***** Un algoritmo es una secuencias de ordenes a la máquina
**** Funcional
***** Ejemplo lisp
***** Simplifica expresiones
**** Orientada a Objetos
***** Ejemplo Simula
***** serie de clases que componen un objeto
**** Logica
***** Ejemplo Prolog
***** Computan información logica
*** Lenguajes populares
**** Java
**** c++
**** Fortran
*** Lenguajes funcionales
**** Capacidad para crear procesos paralelos
**** La utiliza
***** En IA
***** Empresas
**** Se escriben con menos lineas
**** Menores errores
*** Lenguajes pioneros
**** Son los programas con los que estan elaborados otros
@endmindmap
```
## Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación
```plantuml
@startmindmap
* Tendencias actuales en los Lenguajes de Programación y sistemas informaticos
** Sistemas informaticos
** Los Lenguajes van evolucionando
*** Ordenador
**** Facilita el trabajo intelectual
***** Resuelve problemas Mediante algoritmos
*** Proceso de programación
**** Tener un problema
**** Elegir un algortimo que resuelva el problema
** Tendencias actuales
*** En lenguaje ensamblador
**** Se utiliza para
***** Programar perifericos
***** realizar algoritmos
****** Eficientes
****** Rapidos
*** Los paradigmas se acercan a nuestra forma de pensar
*** Multiagentes
**** Toman decisiones
**** Se asocian con otros agentes
**** Son aplicaciones informaticos
*** Ingenieria de software
**** Concepto actual
**** Se encarga de gestionar sistemas complejos
** Proceso de programación
*** Tener un problema
*** Seleccionar un algoritmo que resuelva el programa
*** Codificación en un lenguaje
*** La maquina muestra el resultado
** Paradigma de programación
*** Paradigma
**** Forma de pensar o desarrollar un algoritmo
**** Concibe la programación de una forma determinada
**** Ofrece una conclusión correcta
*** Importancia
**** Para comuniscarse de la forma mas humana posible
****  Representa el lenguaje maquina a palabras que entendemos
*** Tipos
**** Paradigma estructurada
***** Ofrece modularidad
***** Sepera dependencias de las arquitecturas
***** Ejemplos
****** Basic
****** Pascal
****** C
****** Fortran
**** Paradigma funcional
***** Los programas
****** Se comprueban
****** Se verifican
***** Usa funciones matematicas
***** Es recursivo
***** Ejemplos
******  ml
****** Haskel
**** Paradigma Logico
***** Se basa en expresiones logicas
****** Verdadero
****** Falso
*****  Modeliza un problema
***** ejemplos
****** Prolog
****** chaoprolog
**** Paradigma concurrente
***** Es una manera de concebir los problemas
***** Cuando multitudes de usuarios acceden a un recurso
***** Realiza una sincronización
**** Paradigma distribuida
***** Programación para comunicar entre ordenadores
***** Los ordenadores estan alejados
***** Son para programas grandes
****** Se dividen en varios recursos
**** POO
***** Representa objetos
***** Conjunto de clases
**** Programación basada en componentes
***** Reutiliza a los objetos
***** Junta a los objetos para formar un componente
**** Orientada a aspectos
***** Los diferentes aspectos se programan por separado
***** Se juntan los aspectos para formar el sistema
@endmindmap
```

